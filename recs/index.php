<!doctype html>

<html lang="en">
	<head>
			<meta charset="utf-8">
			<meta name="author" content="Milo Szecket">
	        <meta property="og:title" content="dnf's site on the web" />
			<meta property="og:type" content="photo" />
			<meta property="og:url" content="https://dnf.magrittescow.com" />
			<meta property="og:image" content="http://dnf.magrittescow.com/css/assets/thumb.jpg" />
			<meta property="og:description" content="it's got all the links n stuff" />
			<meta name="theme-color" content="#43FFD9">
			<meta property="og:image:height" content="1080">
			<meta property="og:image:width" content="1440">
			<link itemprop="thumbnailUrl" href="http://dnf.magrittescow.com/css/assets/thumb.jpg">
			<span itemprop="thumbnail" itemscope="" itemtype="http://schema.org/ImageObject">
				<link itemprop="url" href="http://dnf.magrittescow.com/css/assets/thumb.jpg">
				<meta itemprop="width" content="1440">
				<meta itemprop="height" content="1080">
			</span>
			<link rel="icon" type="image/gif" href="/css/assets/favicon.gif">
		    <title>recommendations from the guy with the dog</title>
		        

			  <link rel="stylesheet" href="/css/styles.css">
			  <script src="https://cdn.jsdelivr.net/npm/party-js@latest/bundle/party.min.js"></script>
			  <script src="/js/index.js"></script>
			  <script type="module" src="https://cdn.jsdelivr.net/gh/zerodevx/zero-md@2/dist/zero-md.min.js"></script>
			  <script src="js/index.js"></script>

	</head>

	<body>
<div>
		<h1>
			<div class= "header">
			<a href="http://dnf.magrittescow.com"><img class="avatar" src="/css/assets/discord.gif"></a><div> Recommendations 
		<div class="pronouns" >
			<p>he/him/if you want to use they for some reason i won't care</p>
		</div>
	</div>
	</div>
	</h1>
	<div><p class="intro">here you can see the stuff i like!</p></div>

	</div>
	<div class = "recDiv" id="recommendationDiv">
		<?php
			$dir    = './repo/';
			$files = scandir($dir);
			$list = "[";
			foreach($files as $file){
				if(str_contains($file, ".md")){
					$list = $list . '"' . $file . '", ';
				}
			}
			$list = trim($list, ", ") . "]";
		?>
		<script>
			var files = <?php echo $list ?>;
			files.forEach(function(file) {
				document.write("<details class='recDeets'><summary>" + file.substring(0, file.length - 3) + "</summary><div class='recBody'>")
				document.write("<zero-md class='rec' src='repo/" + file + "' manual-render><template><link rel='stylesheet' href='css/rec.css'><link rel='stylesheet' href='/css/styles.css'></template></zero-md>");
				document.write("</div></details>")
			});
			
		</script>
	</div>
	</body>
</html>
