  #easy #story-focused #puzzle #cry #accessibility 

![chicory](chicory.jpeg)

chicory: a colorful tale is an adorable game full of colorful characters - well, however colorful you feel like making them. taking the role of the (not a, *the*) painter of the world, the Wielder of the Paintbrush, you, a little dog, crack puzzles, swim in paint, defeat monsters, solve mysteries, confront your impostor syndrome, attack and dethrone god, start a revolution, meet your heroes, become one yourself, and learn how important the people in your life can be.