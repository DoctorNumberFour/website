#mystery #first-person #hard #story-focused #exploration 

![return of the obra dinn](obradinn.jpg)

return of the obra dinn is another masterpiece from the papers please guy. you play an insurance adjuster trying to figure out What Happened on a boat, using only the ship register, an unlabeled picture of the crew, and a watch that lets you see the moment of a corpse's death.