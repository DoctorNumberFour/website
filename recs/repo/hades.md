#story-focused #hard #accessibility #roguelike

![hades](hades.jpeg)

hades is an incredible implementation of the roguelike genre, with a story spanning hundreds of runs and more dialogue than socrates. battle coworkers, use gods to kill gods, reunite your family. the beautiful art and wonderfully fluid combat grab, and the story bites, latching on like an army ant. kill your dad. again. and again.