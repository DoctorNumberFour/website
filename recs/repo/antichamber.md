#puzzle #exploration #first-person 

![antichamber](antichamber.jpg)

antichamber is an odd, distant puzzler wherein you wake up in an oppressively white room and must navigate a noneuclidean maze of nonsense with *something* lurking in it, while the walls dribble proverbs at you.