

async function render(obj, opts = {}) {
    await obj.waitForReady()
    const stamped = {}
    var pending = (await obj.buildMd(opts)).replaceAll(/&amp;/g , "&").replaceAll(/&#39;/g , "\'").replaceAll(/&quot;/g , "\"").replaceAll(new RegExp('#[a-zA-Z0-9\\-]*', 'gm'), "<span class='tag $&'>$&</span>");
    const css = obj.buildStyles()
    if (css !== obj.cache.styles) {
      obj.cache.styles = css
      await obj.stampStyles(css)
      stamped.styles = true
      await obj.tick()
    }
    var md = pending
    if (md !== obj.cache.body) {
      obj.cache.body = md
      const node = obj.stampBody(md)
      stamped.body = true
      await obj.highlight(node)
    }
    obj.fire('zero-md-rendered', { stamped })
  }



window.onload = (event) => {
	
	const recs = document.querySelectorAll('zero-md');
	const run = async () => {
	  recs.forEach(async rec => {
	  	await render(rec);
	  });
	  	
	}
	run()
};
